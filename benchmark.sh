#!/bin/bash -e

# Script for profiling browser memory and CPU usage using psrecord
# https://github.com/astrofrog/psrecord

# Usage: ./psrecord.sh <application>

HN=https://hacker-news.firebaseio.com/v0

echo "Benchmarking $1"

topstories=$(
  curl -sS "$HN/topstories.json" | tr , "\n" | tr -Cd "[:digit:]\n" | head -20
)

$1 &

psrecord $! --plot plot_$1.png --log activity_$1.txt --include-children --duration 250 &

ps_pid=$!

for id in $topstories
do
  sleep 10
  url=$(
    curl -sS "$HN/item/$id.json?print=pretty" | \
    sed -nE 's/.*"url" : "(.*)".*/\1/p'
  )
  "$1" "$url"
done

wait $ps_pid
