Building the extension
======================

```
git submodule update --init
node convert-declarative-net-request-rules.js {filter-lists...}
```

(Non-request-filters are written to `filters.txt` and can be loaded in ABP
running along the `declarativeNetRequest` test extension.)


Running the benchmark
=====================
First, install the extension you want to benchmark in the browser.
Then, run the benchmark:
```
./psrecord.sh <application>
```
(e.g. `./psrecord.sh google-chrome`)

Requires [`psrecord`](https://github.com/astrofrog/psrecord).<br>
Results print to activity_\<application>.txt and plot_\<activity>.png.